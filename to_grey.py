#!/usr/bin/env python3

from PIL import Image
import sys
import os

def read_image(path):
    im = Image.open(path)


def main():
    if len(sys.argv) != 2:
        print("usage:", sys.argv[0], "image_to_convert")
        return
    filename = sys.argv[1]
    try:
        im = Image.open(filename)
    except FileNotFoundError:
        print("File not found: ", filename)

    im = im.convert("L")
    name, extension = os.path.splitext(filename)
    im.save(name + "_grey.png")

if __name__ == "__main__":
    main()
