install:
	virtualenv env --python=python3.5
	env/bin/pip install -r requirements.txt

uninstall:
	rm -r env

flower:
	env/bin/python src/runner.py source_images/04_flower.png

puppy:
	env/bin/python src/runner.py source_images/07_puppy.png

lena:
	env/bin/python src/runner.py source_images/06_lena_face.png

all:
	env/bin/python src/runner.py all

informe: inf/img
	(cd inf/ && ./gen_images.sh && pdflatex informe && biber informe && pdflatex informe && open informe.pdf)