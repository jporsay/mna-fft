# Useful links
* [Wikipedia DCT](https://en.wikipedia.org/wiki/Discrete_cosine_transform)
* [unix4lyfe dct image compression](https://unix4lyfe.org/dct/)
* [Mathworks dct](http://www.mathworks.com/help/images/ref/dct2.html)
* [Mathworks idct](http://www.mathworks.com/help/images/ref/idct2.html)
# Optimizations
* Fast cosine transform is O(N log(N)) vs DCT O(N^4)
# How to use
        # First time setup:
        git clone git@bitbucket.org:iLNaNo/mna-fft.git
        cd mna-fft
        make install

        # To run the app:
        make run

cheers!

